﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireEnemy : MonoBehaviour
{
    public GameObject bullet;
    public float interval = 1;

    void Start()
    {
        InvokeRepeating("Fire", interval, interval);
    }

    // Update is called once per frame
    void Fire()
    {
        GameObject g=(GameObject)Instantiate(bullet, transform.position, Quaternion.identity);

    }
}
